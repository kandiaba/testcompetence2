import {createAll} from './users.data';
const users = createAll();

console.log('----EXO 1---', users);

// Créer une fonction qui qui prend en paramètre la variable "users",
// un argument "firstName" de type string et qui remplace tous les prénoms
// des users par cet argument et qui met une majuscule à tous les noms et
// prénoms des users
function remplaceUserForName(users, firstName1) {
  return users.map((fam) =>{
    fam.firstName = firstName1;
    fam.firstName = capi(fam.firstName);
    fam.lastName = capi(fam.lastName);
    fam.family.map((user) => {
      user.firstName = firstName1;
      user.firstName = capi(user.firstName);
      user.lastName = capi(user.lastName);
      return user;
    });
    return fam;
  });
}

console.log(remplaceUserForName(users, 'kandiaba'));

function capi(name) {
  return name ? name.charAt(0).toUpperCase() + name.slice(1) : '';
}
