import {createAll} from './users.data';
const users = createAll();

console.log('----EXO 2---', users);

// Créer une fonction qui prend en paramètre la variable "users" et renvoie
// le nombre d'users total en prenant en compte les users qui sont
// dans les attributs 'family'
function reduceUser(users) {
  return users.reduce((acc, user) => [...acc, user.family], []);
}

console.log(reduceUser(users));
console.log(users.reduce(reduceUser));

