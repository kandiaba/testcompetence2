import {createAll} from './users.data';
const users = createAll();

console.log('----EXO 4---', users);

// Créer une fonction qui prend en paramètre la variable "users"
// qui récupères toutes les valeurs stockées dans l'attribut "family" de chaque
// user dans un seul tableau, puis qui mets en majuscule tous leurs
// noms et prénom, puis qui ne garde que les personnes qui n'ont pas de voiture
// et moins de 35 ans
function capi(name) {
  return name ? name.charAt(0).toUpperCase() + name.slice(1) : '';
}
function usersFamilyAgeCar(users) {
  return users.reduce((acc, user) => [...acc, ...user.family], [])
      .filter((user) => user.age <= 35 && user.car === false)
      .map((user) => {
        user.firstName = capi(user.firstName);
        user.lastName = capi(user.lastName);
        return user;
      });
}
console.log(usersFamilyAgeCar(users));

