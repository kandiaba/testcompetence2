import {createAll} from './users.data';

const users = createAll();

console.log('----EXO 6---', users);

// créer une fonction qui prend en paramètre la variable 'users' et qui renvoie
// un nouvel objet dont les attributs sont la concaténation du nom, du prénom et de l'age
// de la personne et dont la valeur est la valeur du boolean "car". Les users qui sont
// dans les attributs "family" doivent être mis au premier rang de cet objet
// voir exemple de résultat ci-dessous

const example = {
  johnDoe32: true,
  BernardMinet45: false,
  alinaChef23: true,
};

console.log(example);

// dans cet exemple la personne Alina Chef était dans l'attribut family de
// la personne Bernard Minet
