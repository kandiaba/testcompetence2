const firstName = [
  'joe',
  'jane',
  'seb',
  'alina',
  'fabien',
  'merwan',
  'anna',
  'annah',
  'Fathma',
  'Mohamed',
  'Walid',
  'Josiane',
  undefined,
];
const lastName = [
  'Dupont',
  'Dupond',
  'Durand',
  'Kassir',
  'Dalachra',
  'Hussein',
  'Wartani',
  'Thoumsi',
  'Angello',
  undefined,
];

function createRandomNumber(min, max) {
  return Math.floor(min + Math.random() * (max - min));
}

function getValue(array) {
  return array[createRandomNumber(0, array.length)];
}

function createUser(end) {
  const tab = [];

  for (let i = 0; i < end; i++) {
    tab.push({
      firstName: getValue(firstName),
      lastName: getValue(lastName),
      age: createRandomNumber(10, 120),
      car: !!createRandomNumber(0, 2),
    });
  }
  return tab;
}

export function createAll() {
  let families = createUser(createRandomNumber(70, 130));
  families = families.map((fam) => {
    fam.family = createUser(createRandomNumber(8, 16));
    return fam;
  });
  return families;
}


